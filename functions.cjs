const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}




function moviesMorethan500M(movies){

    const result = Object.entries(movies).filter(([name, details]) => {
        return details.totalEarnings.replace('$', '').slice(0, 3) > 500;
    })
    return Object.fromEntries(result);
}


console.log(moviesMorethan500M(favouritesMovies));




function oscarNominationAndEarnings(movies){

    const result = Object.entries(movies).filter(([name, details]) => {
        return (details.totalEarnings.replace('$', '').slice(0, 3) > 500) && (details.oscarNominations > 3);
    })

    return Object.fromEntries(result);


}


console.log(oscarNominationAndEarnings(favouritesMovies))





function moviesOfActor(movies){
    
    const result = Object.entries(movies).filter((details) => {
        return details[1].actors.join(",").toLowerCase().includes('leonardo dicaprio');
    })


    return Object.fromEntries(result);

}


console.log(moviesOfActor(favouritesMovies));


///sorting


function grouping(movies){

    let data = Object.entries(movies);
    
    let result = data.reduce((acc,info) => {
        if(acc.hasOwnProperty(info[1].genre)){
            acc[info[1].genre].push(info[0]);
        }
        else{
           acc[ info[1].genre ] = []
           acc[info[1].genre].push(info[0]);
        }

        return acc;

    },[])

    return result;
}


console.log(grouping(favouritesMovies));






















